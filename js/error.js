function launchError(txt,popupOpen) {
    $('#errorPopUp .modal-body').text(txt);
    $('#errorPopUp').modal();
	if(popupOpen != 'null')
		{
		//$('#errorPopUp button').off();
		$('#errorPopUp button').click(function (){
			popupOpen.modal();
			});
		}
    };

function detectError(msg,popupOpen) {
    var ret = true;
    if(msg.indexOf('BD001') != -1)
        {
        launchError('Erreur de connexion à la BDD',popupOpen);
        }
    else if(msg.indexOf('BD002') != -1)
        {
        launchError('Base de données : La table n\'existe pas',popupOpen);
        }
    else if(msg.indexOf('DT001') != -1)
        {
        launchError('La date de départ est supérieur à la date d\'arrivée',popupOpen);
        }
    else if(msg.indexOf('DT002') != -1)
        {
        launchError('Le nombre de places est invalide',popupOpen);
        }
    else if(msg.indexOf('DT003') != -1)
        {
        launchError('Le véhicule selectionné n\'existe pas ou n\'est pas disponnible',popupOpen);
        }
    else if(msg.indexOf('DT004') != -1)
        {
        launchError('La plage de réservation est trop longue',popupOpen);
        }
    else if(msg.indexOf('DT005') != -1)
        {
        launchError('L\'utilisateur n\'existe pas',popupOpen);
        }
    else if(msg.indexOf('DT006') != -1)
        {
        launchError('Le format de données est invalide',popupOpen);
        }
    else if(msg.indexOf('DT007') != -1)
        {
        launchError('Le kilométrage de départ est supérieur au kilométrage d\'arrivée',popupOpen);
        }
    else if(msg.indexOf('AU001') != -1)
        {
        launchError('Accès non autorisé',$('#LoginPopUp'));
        }
    else if(msg.indexOf('AU002') != -1)
        {
        launchError('Connexion échouée',$('#LoginPopUp'));
        }
    else if(msg.indexOf('AU003') != -1)
        {
        launchError('Connexion expiré',$('#LoginPopUp'));
        }
    else
        {
        ret = false;                
        }
    return ret;
    }